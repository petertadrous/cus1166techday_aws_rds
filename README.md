# RDS Important Links:
Set up: https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_SettingUp.html

Getting started with MariaDB: https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_GettingStarted.CreatingConnecting.MariaDB.html

Getting started with Microsoft SQL Server: https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_GettingStarted.CreatingConnecting.SQLServer.html

Getting started with MySQL: https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_GettingStarted.CreatingConnecting.MySQL.html

Getting started with Oracle: https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_GettingStarted.CreatingConnecting.Oracle.html

Getting started with PostgreSQL: https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_GettingStarted.CreatingConnecting.PostgreSQL.html


# MySQL Workbench Links:
MySQL Workbench Download: http://dev.mysql.com/downloads/workbench/

MYSQL Workbench Documentation: https://dev.mysql.com/doc/workbench/en/

MySQL Workbench Tutorial: https://www.youtube.com/watch?v=W98sn6oMtYw


# To connect to my database:
Host name: mysql-instance1.co3fdmvb9pke.us-east-2.rds.amazonaws.com

Username: mysqladmin

Password: Mysqlpassword1

UPDATE: I took down my database so I don't get charged by amazon. To try a connection, follow the tutorials to create your own database.